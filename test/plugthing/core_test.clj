(ns plugthing.core-test
  (:require
    [clojure.test   :refer :all]
    [clojure.string :as s]

    ; [plugthing.core :refer :all]
    [plugthing.board :refer :all]
    [plugthing.render :refer :all]
    [plugthing.moves :refer :all]

    [mascip.prompt :refer :all]
    [mascip.string :refer :all]
    [mascip.map :refer :all]
    )
  )

; TODO: test prompts by mocking input

;;; Helper Functions
(defn ^String substring?
  "True if s contains the substring."
  [substring ^String s]
  (.contains s substring))

;;; Create Board
(deftest create-a-board-of-size-1
  (is (= {{:row 0, :col 0} {:plugged true}} (create-board 1))))

(deftest create-a-board-of-size-2
  (is (= { {:row 0, :col 0} {:plugged true},
           {:row 1, :col 0} {:plugged true},
           {:row 1, :col 1} {:plugged true}}
         (create-board 2))))

;;; Board positions
(def letters [\a \b \c \d])
(def positions
  [ {:row 0, :col 0}
    {:row 1, :col 0}
    {:row 1, :col 1}
    {:row 2, :col 0}])

(deftest test-pos->letter
  (is (= letters
         (map pos->letter positions))))

(deftest test-letter->pos
  (is (= positions
         (map letter->pos letters))))

;;; Render
(deftest pre-rendering-a-board-size-3
  (is (= [ [[\a true]]
           [[\b true] [\c true]]
           [[\d true] [\e true] [\f true]]] 
         (pre-render (create-board 3)))))

(deftest rendering-a-plugged-hole
  (is (= (str "a" (colorize "0" :blue))
         (render-hole [\a true]))))

(deftest rendering-an-empty-hole
  (is (= (str "b" (colorize "-" :red))
         (render-hole [\b false]))))

;;; Moves
(deftest remove-a-plug
  (is (= [[[\a false]]]
         (pre-render (remove-plug (create-board 1) {:row 0, :col 0})))))
