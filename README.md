# plugthing

The PlugThing game!
Inspired by the Clojure tutorial and book called Clojure for the Brave and True,
and modified to my liking as a training to the Clojure language.

## Usage

To start playing:
$ lein run

## License


Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
