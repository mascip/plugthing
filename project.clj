(defproject plugthing "0.1.0-SNAPSHOT"
  :description "Plug Thing game from the Brave and True Clojure tutorial"
  :url ""
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [
    [org.clojure/clojure "1.6.0"]
    [expectations "2.0.6"]
    [medley "0.5.0"]
    [prismatic/plumbing "0.3.3"]
  ]
  :plugins [
    [lein-autoexpect "1.2.2"]
    [lein-kibit "0.0.8"]
  ]
  ; :main ^:skip-aot user/-the-main
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}} 
)
