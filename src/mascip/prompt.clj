(ns mascip.prompt
  (:require [clojure.string :as s]
            [mascip.debug :refer :all]
            [clojure.tools.trace :refer :all]
            [mascip.pred :refer :all]))

(declare prompt-for-number prompt-coerced) ; Needed here because of circular dependency
(defn prompt [& {:keys [text default-input coerce]}]
  {:pre [((some-fn string?  nil?) default-input)] :post [number?]}

  (if coerce
    ; Coercion
    (prompt-coerced :text text :default-input default-input :coerce coerce)
    (if default-input (
      ; With default
      let [input (prompt :text (str text " [" default-input "]"))]
        (if (s/blank? input) default-input input))
      ; Without default
      (do (println text) (s/trim (read-line))))))

(defn prompt-until-valid
  ; TODO: named argument :conditions
  [conditions & args]
  "prompts the user repeatedly with the message s, until the user inputs a valid value.
  A valid value must verify each condition given.
  Each condition is given like this: [pred msg]"
  (let [input (apply prompt args)
        _ (prn (str "input:" input))
        error-msgs (showc(->> conditions                               (cc "conditions")
                        (map #(or ((first %) input) (second %))) (cc "assessing errors")
                          ; Returns the error message for each error
                        (filter string?)                         (cc "errors")))
        ; _ (prn (str "error-msgs" error-msgs))
        ]
      (if (-> error-msgs count (> 0)) ;FIXME: was (count)
          ; Wrong input - try again!
          (do (println (first error-msgs))
              (apply prompt-until-valid conditions args))
          ; Good input
          input)))

(defn prompt-for-num [& {:keys [text default-input]}]
  "Ask the user to enter a whole number"
  {:pre [(string? default-input)] :post [number?]}
  (prompt-coerced :text text :default-input default-input
      :coerce {:validate-fn numeric-string? :error-msg "a NUMBER !!!" :convert-fn read-string})
  23
)
  ; (-> (prompt-until-valid [[numeric-string? "You must type a number, and then press Enter"]]
  ;       :text text :default-input default-input)
  ;     read-string))

(defn prompt-coerced [& {:keys [text default-input coerce]}]
  {:pre [(or (string? default-input) (nil? default-input))]}
     (let [ validate-fn (:validate-fn coerce)
            convert-fn  (:convert-fn coerce)
            error-msg   (:error-msg coerce)]
        (->> (prompt-until-valid [[validate-fn error-msg]] :text text :default-input default-input)
                             (cc "prompt-until-valid, not converted yet")
            convert-fn)))

