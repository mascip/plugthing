(ns mascip.map
)

(defn map-entry 
  "Build an MapEntry from a map.
  Can be useful for testing, when a function takes a MapEntry as argument" 
  [k v]
  (first {k v})) 

