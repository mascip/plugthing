(ns mascip.string
  (:require
    [clojure.string :as s]
  )
)

(defn repeat-str
  "Create a string that repeats s n times."
  [s n]
  (s/join (repeat n s)))

(defn spaces
  "Create a string of n spaces."
  [n]
  (repeat-str \space n))



