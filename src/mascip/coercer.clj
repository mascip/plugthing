(ns mascip.coercer
  (:require
    [mascip.pred :refer :all]
  )
  (:refer-clojure :exclude [int])
)

(def an-int {:validate-fn numeric-string? :error-msg "You must enter a NUMBER" :convert-fn read-string})

(def a-char {:validate-fn string? :error-msg "You must enter a CHARACTER" :convert-fn first})

