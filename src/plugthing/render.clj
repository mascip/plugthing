(ns plugthing.render
  (:require
    [plugthing.board :refer :all]

    [clojure.string :as s]

    [plumbing.core :refer [grouped-map map-vals]]
    [mascip.string :as m.s]
    [mascip.debug :refer :all]
))

(defn pre-render [board]
  "A pre-rendered board with 2 rows looks like this:
   [
     [[\\a true]]
     [[\\b true] [\\c true]]
   ]"
  ; TODO: with partition-by, and map-on-leaves??
  (->> board
    (grouped-map #(h->row %)
                 (fn [[pos data]] [(pos->letter pos) (:plugged data)])) (cc "group rows and transform")
    vals                          (cc "extract the rows only")
    (sort-by count)               (cc "sort by row number")
    (map (partial sort-by first)) (cc "sort by char")
    ; (sort-by first)               (cc "sort by char")
  ))

(def ansi-styles {:red   "[31m", :green "[32m", :blue  "[34m", :reset "[0m"})

(defn apply-ansi
  "Produce a string which will apply an ansi style"
  [style]
  (str \u001b (style ansi-styles)))

(defn colorize
  "Apply ansi color to text"
  [text color]
  (str (apply-ansi color) text (apply-ansi :reset)))
  ; text) ; For debug: no colors

(defn render-hole
  ([pre-rendered-hole] (render-hole pre-rendered-hole false))
  ([pre-rendered-hole simple]
  "Using pre-rendered data"
  (let [a-char  (first  pre-rendered-hole)
        plugged (second pre-rendered-hole)
        symbol-simple   {true "0", false "-"}
        symbol-colored  {true (colorize "0" :blue), false (colorize "-" :red)}
        symbol-used-now (if simple symbol-simple symbol-colored)]
    (str a-char (symbol-used-now plugged)))))

(defn render-row [board pre-rendered-row simple]
  (let [padding (if simple ""
                           (m.s/spaces (* (- (nb-rows-of board) (count pre-rendered-row)))))]
    (->> pre-rendered-row                 (cc "pre-rendered-row")
         (map #(render-hole % simple))    (ccl "rendered")
         (concat [padding])               (cc "add padding")
         (s/join " ")                     (cc "join")
         str                              (cc "str")
         )))

(defn rows-range [board] (range (nb-rows-of board)))

(defn render-board
  "Return the board as a String"
  ([board] (render-board board false))
  ([board simple]
    (->> board                              (cc "board")
         pre-render                         (cc "pre-rendered")
         (map #(render-row board % simple)) (ccl "rendered rows")
         (s/join "\n" )                     (cc "join rows")
    )))

