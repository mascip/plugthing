(ns plugthing.board
  (:require
    [mascip.debug :refer [c cc]]

    [medley.core :refer :all :only [find-first]]
  )
)

;;;; Positions
; Every position is represented as {:row 1, :col 3}, and also has an ID, and a letter representation
; A hole is a map of positions to information, for example: {{:row 0, :col 0} {:plugged true}}

(defn pos [row col] {:row row :col col})

(defn pos->id
  "{:row 0, :col 0} -> 0.
  For the other positions, move right and increment each time.
  When reaching the end of a row, increment and go the the first element of the next line.
  {:row 1, :col 0} -> 1
  {:row 1, :col 1} -> 2
  {:row 2, :col 0} -> 3
  etc"
  [{row :row, col :col}]
  ; Each extra column gives + 1, Each extra row gives + row
  (+ col  ; + 1 per column
     (reduce + (range (inc row))))) ; sum up rows increments

(defn char-range [start-char end-char]
  "Example:
  (char-range \\a \\d)
  => (\\a \\b \\c \\d)"
  (map char (range (int start-char) (inc (int end-char)))))

(defn pos->letter
  "Each position is represented by a letter. Read the doc for (pos->id)"
  [pos]
  (->> pos (pos->id) (nth (char-range \a \z))))

(defn next-pos [{row :row, col :col}]
  (if (< col row)
    {:row row,      :col (inc col)} ; Next column
    {:row (inc row) :col 0}))       ; New row

(def infinite-positions (iterate next-pos {:row 0, :col 0}))

(defn id->pos
  "Position that corresponds to a given ID. Inverse of pos->id (read its doc)"
  [id]
  (find-first (comp (partial = id) pos->id) infinite-positions))

(defn position-in-alphabet-vector [letter]
  (- (int letter) (int \a)))

(defn letter->pos
  "A letter represents a position. Read the doc for (pos->id)"
  [letter]
  (->> letter
       (position-in-alphabet-vector)
       (id->pos)))

;;;;
;;;; Constructor

(defn create-hole 
  "Each hole is represented as follows:
  {{:row 0, :col 0} {:plugged true}}
  The key is a position, and the value is the data"
  [row col]
  {(pos row col) {:plugged true}})

(defn create-row [row-num]
  (->> (range (inc row-num)) ; On row N there are N + 1 holes
       (map #(create-hole row-num %))))

(defn create-board
  "A board is a list of holes"
  [nb-rows]
  (apply merge (mapcat #(create-row %) (range nb-rows))))

;;;; Get info about the board

(defn all-pos [board] (keys board))

(defn nb-rows-of [board] 
  (->> (all-pos board) (map :row) (apply max) (inc)))

(defn valid-pos? [board pos]
  (and (< -1 (:row pos) (nb-rows-of board))
       (< -1 (:row pos) (nb-rows-of board))))

; hole -> potition
(defn h->pos [hole] (key hole))
(defn h->row [hole] (->> hole h->pos :row))
(defn h->col [hole] (->> hole h->pos :col))

(defn h->plugged? [hole] (-> hole val :plugged))
