(ns plugthing.prompt
  (:require
    [plugthing.board :refer :all]

    [mascip.prompt :refer :all]
    [mascip.pred :refer :all]
    [mascip.coercer :as m.coercer]
  )
)

(defn prompt-for-nb-rows [default]
  {:pre [(number? default)] :post [number?]}

  (prompt-until-valid [
    [(every-pred  (partial <= 3)(partial >= 6))
     "The number of rows must be between 3 and 6"]]
    :text "How many rows?" :default-input (str default) :coerce m.coercer/an-int))

(defn prompt-for-initial-plug-to-remove [board default]
  {:pre [(char? default)] :post [char?]}
  (prompt-until-valid [
    ;TODO the condition should be a map rather than a vector??
    [(comp (partial valid-pos? board) letter->pos)
    "You must choose a letter representing one of the holes"]]
    :text "Choose a plug to remove" :default-input (str default) :coerce m.coercer/a-char))

(def coercer-two-chars {:validate-fn (every-pred #(= 2 (count %)) #(every-pred char? (juxt first second %))) :error-msg "You must enter a MOVE, consisting of two valid characters. Eg: ad" :convert-fn (partial map char)})

; TODO: I need a function valid-move?
(defn prompt-for-move [board]
  (prompt-until-valid [
    [#(every? (comp (partial valid-pos? board) letter->pos) %)
     "This move is not valid."]]
    :text "Choose a move:" :coerce coercer-two-chars))
