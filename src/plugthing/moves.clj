(ns plugthing.moves
  (:require
    [mascip.debug :refer :all]

    [plugthing.board    :refer  :all]
    )
  )

(defn remove-plug [board pos]
  (assoc-in board [pos :plugged] false))

(defn remove-plug-at-letter [board letter]
  (->> letter                        (cc "letter")
       (letter->pos)                 (cc "pos")
       ((partial remove-plug board)) (cc "board")))
