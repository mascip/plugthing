(ns plugthing.core
  (:require
    [plugthing.board :refer :all]
    [plugthing.moves :refer :all]
    [plugthing.prompt :refer :all]
    [plugthing.render :refer :all]
    ))

;;;; CONFIG
(def default-nb-rows 5)
(def default-first-move \e)

(defn print-board [board]
  (println (render-board board))
  board)

; (defn prompt-for-initial-plug-to-remove [c] c)


;;;; MAIN
(defn -main
  "To play PegThing, run this method with 'lein run'"
  []
  (println "Get Ready to play PegThing!")

        ; A new board: prompt for board size, create and display
  (as-> (->> (prompt-for-nb-rows default-nb-rows)
             (create-board)) B
        (print-board B)

        ; Prompt for initial plug to remove
        (->> (prompt-for-initial-plug-to-remove B default-first-move)
             (remove-plug-at-letter B))
        (print-board B)

        ; Play ! (not implemented yet)
        (loop [board B]
          (-> (prompt-for-move B) ; TODO: first, check (valid-move?) Later I could do (valid-moves-are)
              ; (move-plug B)
              ; (if (game-finished? B)
              ;   (display-result B)
              ;   (recur B))

              ))

 )  ; End as-> Board
) ; End -main

; (defn -main-without-threading
;   "To play PegThing, run this method with 'lein run'"
;   []
;   (println "Get Ready to play PegThing!")

;   ; A new board: prompt for board size, create and display
;   (def initial-board
;     (-> (prompt-for-nb-rows default-nb-rows)
;         (create-board)))
;   (print-board initial-board)

;   ; Prompt for initial plug to remove
;   (def board-less-initial-plug
;     (as-> initial-board B
;         (prompt-for-initial-plug-to-remove B default-first-move)
;         (remove-plug-at-letter B)))
;   (print-board board-less-initial-plug)

;   ; Play ! (not implemented yet)
;   (loop [board board-less-initial-plug]
;     (as-> board B
;           (prompt-for-move B)
;           (move-plug B)
;           (if (game-finished? B)
;             (display-result B)
;             (recur B))))
; )
