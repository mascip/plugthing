(ns user
  (:require
    [plugthing.core   :refer :all]
    [plugthing.board  :refer :all]
    [plugthing.render :refer :all]
    [plugthing.prompt :refer :all]
    [plugthing.moves  :refer :all]

    [mascip.coercer   :refer :all]
    [mascip.map       :refer :all]
    [mascip.pred      :refer :all]
    [mascip.prompt    :refer :all]
    [mascip.string    :refer :all]
  )
)

; I removed this, so it will work on my Windows laptop in Cursive
;(load-file "/home/user/.lein/my-repl-init.clj")

; (defn -the-main [] (-main))
